#!/bin/sh
set -eux

# from standard-test-source
SOURCE="$(pwd)/source"
LOGS="$(pwd)/logs"
TESTS="$(pwd)/tests"
mkdir -p "$LOGS"
chmod a+w "$LOGS"

# moving SOURCE out of the way.
# Will use upstream source for tests
if [ -d "$SOURCE" ]; then
    mv ${SOURCE} ${SOURCE}_str
fi

# install browser; on RHEL, use chromium from epel
# HACK: chromium-headless ought to be enough, but version 88 has a crash: https://bugs.chromium.org/p/chromium/issues/detail?id=1170634
if ! rpm -q chromium; then
    if grep -q 'ID=.*rhel' /etc/os-release; then
        dnf install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
        dnf config-manager --enable epel
        # Force epel to 8 since epel 9 does not exist yet
        sed -i 's/$releasever/8/g' /etc/yum.repos.d/epel*
    fi
    dnf install -y chromium
    dnf remove -y epel-release
fi

# install cockpit-packagekit and glibc-langpack-en for testAppMenu
dnf install -y cockpit-packagekit glibc-langpack-en

# create user account for logging in
if ! id admin 2>/dev/null; then
    useradd -c Administrator -G wheel admin
    echo admin:foobar | chpasswd
fi

# avoid sudo lecture during tests
su -c 'echo foobar | sudo --stdin whoami' - admin

# create user account for running the test
if ! id runtest 2>/dev/null; then
    useradd -c 'Test runner' runtest
    # allow test to set up things on the machine
    mkdir -p /root/.ssh
    curl https://raw.githubusercontent.com/cockpit-project/bots/master/machine/identity.pub  >> /root/.ssh/authorized_keys
    chmod 600 /root/.ssh/authorized_keys
fi

# Use upstream code for tests:
git clone https://github.com/Scribery/cockpit-session-recording.git "$SOURCE"
chown -R runtest "$SOURCE"

# disable core dumps, we rather investigate them upstream where test VMs are accessible
echo core > /proc/sys/kernel/core_pattern

# CSR specific setup
mkdir -p /var/log/journal/
cp 1.journal /var/log/journal/1.journal
cp binary-rec.journal /var/log/journal/binary-rec.journal
usermod -u 981 tlog

# Run tests as unprivileged user
su - -c "env SOURCE=$SOURCE LOGS=$LOGS $TESTS/run-test.sh" runtest


RC=$(cat $LOGS/exitcode)
exit ${RC:-1}
